const { Schema, model } = require('mongoose');

const GallerySchema =  Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    slug: {
        type: String,
    },
    images: [{
        type: String,
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    },
});

GallerySchema.methods.toJSON = function () {
    const { ...model } = this.toObject();
    return model;
}

module.exports = model('Gallery',  GallerySchema);
