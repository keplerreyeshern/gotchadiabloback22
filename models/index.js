const Gallery = require('./gallery.model');
const Package = require('./package.model');
const PasswordReset = require('./password_reset.model');
const Role = require('./role.model');
const User = require('./user.model');
const Server = require('./server');

module.exports = {
    Gallery,
    Package,
    PasswordReset,
    Role,
    User,
    Server,
}
