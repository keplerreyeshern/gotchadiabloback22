const { Schema, model } = require('mongoose');

const PackageSchema =  Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    description: {
        type: String,
    },
    slug: {
        type: String,
        required: [true, 'El slug es obligatorio']
    },
    price: {
        type: Number,
    },
    bullets: {
        type: Number,
    },
    image: {
        type: String,
    },
    images: [{
        type: String,
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    },
});

PackageSchema.methods.toJSON = function () {
    const { ...model } = this.toObject();
    return model;
}

module.exports = model('Package',  PackageSchema);
