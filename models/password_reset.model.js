const { Schema, model } = require('mongoose');

const PasswordResetSchema = Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'El usuario es obligatorio']
    },
    token: {
        type: String,
        required: [true, 'El token es obligatorio']
    }
});

module.exports = model('PasswordReset', PasswordResetSchema);
