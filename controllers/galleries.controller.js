const { request, response } = require('express');
const { Gallery } = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUpload } = require("../helpers");


const galleriesGet = async (req, res = response) => {

    const [ total, galleries ] = await Promise.all([
        Gallery.countDocuments({delete:false}),
        Gallery.find({delete:false})
    ]);

    res.json({
        total,
        galleries
    });
};


const galleryGet = async (req, res = response) => {

    const id = req.params.id;
    const [ gallery ] = await Promise.all([
        Gallery.findById(id)
    ]);


    res.json({
        gallery
    });
};


const getGalleries = async (req, res = response) => {

    const [ total, galleries ] = await Promise.all([
        Gallery.countDocuments({delete:false, active:true}),
        Gallery.find({delete:false, active:true})
    ]);

    res.json({
        total,
        galleries
    });
};


const getGallery = async (req, res = response) => {

    const slug = req.params.slug;
    const [ gallery ] = await Promise.all([
        Gallery.findOne({slug})
    ]);


    res.json({
        gallery
    });
};

const galleryPost = async (req = request, res = response) => {

    let { name } = req.body;
    const slug = urlSlug(name);
    const existGallery = await Gallery.findOne({slug});
    if (existGallery){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre de la galeria`
        })
    }
    const gallery = new Gallery({name});

    // Guardar en Base de datos
    await gallery.save();

    res.json({
        gallery
    })
}

const galleryActive = async (req = request, res = response) => {

    const id = req.params.id;
    let { option } = req.body;
    const gallery = await Gallery.findByIdAndUpdate(id, {active: option},{new: true});

    res.json({
        gallery
    })
}

const galleryPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, description, bullets, price } = req.body;
    const slug = urlSlug(name);
    const existGallery = await Gallery.findOne({slug});
    if (existGallery && existGallery._id != id){
        return res.status(400).json({
            msg: `El slug "${slug.toUpperCase()}" no se puede repetir cambiar el nombre del paquete`
        })
    }
    let image;
    if(req.files){
        image = await fileUpload(req.files, undefined, 'galleries');
    }
    const gallery = await Gallery.findByIdAndUpdate(id, {name, slug, description, bullets, price, image },{new: true});
    res.status(201).json({
        gallery
    });
}

const galleryDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [gallery] = await Promise.all([
        Gallery.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);


    res.json({
        gallery,
    })
}

module.exports = {
    galleriesGet,
    galleryGet,
    getGalleries,
    getGallery,
    galleryPost,
    galleryPut,
    galleryDelete,
    galleryActive
}
