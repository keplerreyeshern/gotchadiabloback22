const { request, response } = require('express');
const { Package } = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUpload } = require("../helpers");


const packagesGet = async (req, res = response) => {

    const [ total, packages ] = await Promise.all([
        Package.countDocuments({delete:false}),
        Package.find({delete:false})
    ]);

    res.json({
        total,
        packages
    });
};


const packageGet = async (req, res = response) => {

    const id = req.params.id;
    const [ package ] = await Promise.all([
        Package.findById(id)
    ]);


    res.json({
        package
    });
};


const getPackages = async (req, res = response) => {

    const [ total, packages ] = await Promise.all([
        Package.countDocuments({delete:false, active:true}),
        Package.find({delete:false, active:true})
    ]);

    res.json({
        total,
        packages
    });
};


const getPackage = async (req, res = response) => {

    const slug = req.params.slug;
    const [ package ] = await Promise.all([
        Package.findOne({slug})
    ]);


    res.json({
        package
    });
};

const packagePost = async (req = request, res = response) => {

    let { name, description, bullets, price } = req.body;
    const slug = urlSlug(name);
    const existPackage = await Package.findOne({slug});
    if (existPackage){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre del paquete`
        })
    }

    let image;
    if(req.files){
        console.log(req.files)
        image = await fileUpload(req.files, undefined, 'packages');
    }
    let data = {name, slug, description, bullets, price, image };
    const package = new Package(data);

    // Guardar en Base de datos
    await package.save();

    res.json({
        package
    })
}

const packageActive = async (req = request, res = response) => {

    const id = req.params.id;
    let { option } = req.body;
    const package = await Package.findByIdAndUpdate(id, {active: option},{new: true});

    res.json({
        package
    })
}

const packagePut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, description, bullets, price } = req.body;
    const slug = urlSlug(name);
    const existPackage = await Package.findOne({slug});
    if (existPackage && existPackage._id != id){
        return res.status(400).json({
            msg: `El slug "${slug.toUpperCase()}" no se puede repetir cambiar el nombre del paquete`
        })
    }
    let image;
    if(req.files){
        image = await fileUpload(req.files, undefined, 'packages');
    }
    const package = await Package.findByIdAndUpdate(id, {name, slug, description, bullets, price, image },{new: true});
    res.status(201).json({
        package
    });
}

const packageDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [package] = await Promise.all([
        Package.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);


    res.json({
        package,
    })
}

module.exports = {
    packagesGet,
    packageGet,
    getPackages,
    getPackage,
    packagePost,
    packagePut,
    packageDelete,
    packageActive
}
