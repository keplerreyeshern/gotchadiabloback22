const { request, response } = require('express');
const { User, PasswordReset, Company } = require('../models');
const bcryptjs = require('bcryptjs');
const { generateJWT } = require("../helpers/generate-jwt");
const { googleVerify } = require("../helpers/google-verify");


const login = async (req = request, res = response) => {

    const { email, password } = req.body;

    try {
            // Verificar si el email existe
        const user = await User.findOne({email});
        if(!user){
            return res.status(400).json({
                msg: " Usuario / Password no son correctos"
            });
        }

        // Verificar si el usuario esta activo
        if(!user.active){
            return res.status(400).json({
                msg: " Usuario / Password no son correctos"
            });
        }

        // Verificar contraseña
        const validPassword = bcryptjs.compareSync(password, user.password);
        if (!validPassword){
            return res.status(400).json({
                msg: " Usuario / Password no son correctos"
            });
        }

        // Generar JWT
        const token = await generateJWT(user._id);

        res.json({
            user,
            token
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            msg: 'Se encontro un error, comuniquese con el administrador'
        });
    }
};

const googleSignIn = async (req = request, res = response) => {
    const { id_token } = req.body;

    try {
        const { name, image, email } = await googleVerify(id_token);
        let user = await User.findOne({email});
        if(!user){
            const data = {
                name,
                image,
                email,
                password: ':P',
                google: true
            };
            user = new User(data);
            await user.save();
        }
        if(!user.active){
            return res.status(401).json({
                msg: 'El usuario esta bloqueado, contacte al administrador'
            });
        }
        // Generar JWT
        const token = await generateJWT(user._id);

        res.json({
            user,
            token
        });
    } catch (error){
        console.log(error);
        res.status(400).json({
            msg: 'Token de Google no es válido'
        });
    }
};

const createToken = async (req = request, res = response) => {

    const { email } = req.body;

    try {
        // Verificar si el email existe
        const user = await User.findOne({email});
        if(!user){
            return res.status(400).json({
                msg: " Usuario no existe en la base de datos"
            });
        }

        // Verificar si el usuario esta activo
        if(!user.active){
            return res.status(400).json({
                msg: " Usuario no tiene permiso para ingresar a la aplicación"
            });
        }
        const token = setToken();

        const passwordReset = new PasswordReset({user, token});

        await passwordReset.save();

        res.json({
            user,
            passwordReset
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            msg: 'Se encontro un error, comuniquese con el administrador'
        });
    }
};


const verifyPassword = async (req = request, res = response) => {

    const id = req.params.id;
    const { password } = req.body;

    const user = await User.findById(id);
    try {
        // Verificar contraseña
        const validPassword = bcryptjs.compareSync(password, user.password);
        if (!validPassword){
            return res.status(400).json({
                msg: "La contraseña no coincide"
            });
        }

        res.json({
            user
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            msg: 'Se encontro un error, comuniquese con el administrador'
        });
    }
};

function random() {
    return Math.random().toString(36).substr(2);
};

function setToken() {
    return random() + random();
};

module.exports = {
    login,
    googleSignIn,
    verifyPassword
};
