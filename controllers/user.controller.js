const { request, response } = require('express');
const { User } = require('../models');
const bcryptjs = require('bcryptjs');


const usersGet = async (req, res = response) => {

    const [ total, users ] = await Promise.all([
        User.countDocuments({active:true}),
        User.find({active:true})
    ]);

    res.json({
        total,
        users
    });
};

const userGet = async (req, res = response) => {

    const id = req.params.id;
    const [ user ] = await Promise.all([
        User.findById(id).populate('groups')
    ]);


    res.json({
        user
    });
};

const userPost = async (req = request, res = response) => {

    let { name, email, password, role } = req.body;
    let data = {name, email, password, role, forcePass:false};
    const user = new User(data);

    // Encriptar contraseñana
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);

    // Guardar en Base de datos
    await user.save();

    res.json({
        user
    })
}



const passwordPost = async ( req = request, res = response) => {
    const id = req.params.id;
    let { password } = req.body;


    //  validar Contra base de datos
    //     // Encriptar contraseñana
    const salt = bcryptjs.genSaltSync();

    const user = await User.findByIdAndUpdate(id, { password:bcryptjs.hashSync(password, salt) }, {new: true});

    res.status(201).json({
        user
    });
}

const userPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { username, name, email, password, company, role, forcePass, expirationDate, blockedDate, blocked, blockingReason, groups } = req.body;
    const companyDB = await Company.findById(company);
    if (!role){
        if (companyDB.name == 'Admin'){
            role = 'SUPER_ADMIN_ROLE';
        } else {
            role = 'ADMIN_ROLE';
        }
    }

    //  validar Contra base de datos
    // if(password){
    //     // Encriptar contraseñana
    //     const salt = bcryptjs.genSaltSync();
    //     rest.password = bcryptjs.hashSync(password, salt);
    // }

    const user = await User.findByIdAndUpdate(id, {username, name, email, role, company:companyDB,
        forcePass, expirationDate, blockedDate, blocked, blockingReason, groups: JSON.parse(groups) },
        {new: true});
    res.status(201).json({
        user
    });
}

const userDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [user] = await Promise.all([
        User.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        user,
        authenticatedUser
    })
}

module.exports = {
    usersGet,
    userGet,
    userPost,
    userPut,
    userDelete,
    passwordPost
}
