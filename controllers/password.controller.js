const { request, response } = require('express');
const { User, Company, Group} = require('../models');
const bcryptjs = require('bcryptjs');


const userGet = async (req, res = response) => {

    const [ total, users ] = await Promise.all([
        User.countDocuments({active:true}),
        User.find({active:true})
    ]);

    res.json({
        total,
        users
    });
};

const getUser = async (req, res = response) => {

    const id = req.params.id;

    const [ user ] = await Promise.all([
        User.findById(id),
    ]);

    res.json({
        user
    });
};

const passwordPost = async (req = request, res = response) => {

    let { id, password } = req.body;
    const user = await User.findById(id);

    // Encriptar contraseñana
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);
    user.forcePass = false;

    // Guardar en Base de datos
    await user.save();

    res.json({
        user
    })
}

const userPut = async ( req = request, res = response) => {
    const id = req.params.id;
    const { _id, password, google, role, ...rest } = req.body;

    //  validar Contra base de datos
    if(password){
        // Encriptar contraseñana
        const salt = bcryptjs.genSaltSync();
        rest.password = bcryptjs.hashSync(password, salt);
    }

    const user = await User.findByIdAndUpdate(id, rest, {new: true});
    res.status(201).json({
        user
    });
}

const userDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [user] = await Promise.all([
        User.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        user,
        authenticatedUser
    })
}

module.exports = {
    userGet,
    passwordPost,
    userPut,
    userDelete,
    getUser
}
