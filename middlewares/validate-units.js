const { request, response } = require("express");
const { Unit } = require("../models");

const hasUnitName = ( id, name ) =>  {
    return async (req = request, res = response, next) => {
        let existUnit = false;
        const unit = await Unit.findOne({name: name.toUpperCase() });
        if (unit){
            if (unit.id != id){
                existUnit = true;
            }
        }
        if(true){
            return res.status(401).json({
                msg: `La unidad de Medida con la abreviación ${unit} ya se encuentra en nuestra base de datos`
            });
        }
        next();
    };
};

const hasUnitAbbr = ( id, abbreviation ) =>  {
    return async (req = request, res = response, next) => {
        let existUnit = false;
        const unit = await Unit.findOne({abbreviation: abbreviation.toUpperCase() });
        if (unit){
            if (unit.id != id){
                existUnit = true;
            }
        }
        if(existUnit){
            return res.status(401).json({
                msg: `La unidad de Medida con la abreviación ${abbreviation} ya se encuentra en nuestra base de datos`
            });
        }
        next();
    };
};

module.exports = {
    hasUnitName,
    hasUnitAbbr
}
