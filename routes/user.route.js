const { Router} = require('express');
const { usersGet, userGet, userDelete, userPut, userPost,passwordPost} = require('../controllers/user.controller');
const { check } = require('express-validator');
const { validateFields, validateJWT, adminRole, hasRole } = require('../middlewares')
const { roleValidate, existEmail, existUserForId } = require("../helpers/db-validators");

const router = Router();

router.get('/', [
    validateJWT,
    validateFields,
], usersGet);

router.post('/', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('password', 'La contraseña debe tener minimo 8 caracteres').isLength({min:8}),
    check('email', 'El correo no es valido').isEmail(),
    validateFields
], userPost);


router.post('/password/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existUserForId),
    check('password', 'La contraseña debe tener minimo 8 caracteres').isLength({min:8}),
    validateFields
], passwordPost);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existUserForId),
    validateFields
], userGet);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existUserForId),
    validateFields
], userPut);

router.delete('/:id', [
    validateJWT,
    // adminRole,
    hasRole('ADMIN_ROLE', 'SALES_ROLE'),
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existUserForId),
    validateFields
], userDelete);

module.exports = router;
