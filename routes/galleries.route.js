const { Router} = require('express');
const { getGalleries, getGallery, galleriesGet, galleryGet, galleryDelete, galleryPut, galleryPost, galleryActive } = require('../controllers/galleries.controller');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares')
const { existGalleryForId } = require("../helpers");

const router = Router();

router.get('/', [
    validateJWT,
    validateFields,
], galleriesGet);

router.get('/public', getGalleries);

router.get('/public/:slug', [
    check('slug', 'El slug es obligatorio').not().isEmpty(),
    validateFields,
], getGallery);

router.post('/', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], galleryPost);

router.post('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existGalleryForId),
    validateFields
], galleryActive);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existGalleryForId),
    validateFields
], galleryGet);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existGalleryForId),
    validateFields
], galleryPut);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existGalleryForId),
    validateFields
], galleryDelete);

module.exports = router;
