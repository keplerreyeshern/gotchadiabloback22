const { Router} = require('express');
const { getPackages, getPackage, packagesGet, packageGet, packageDelete, packagePut, packagePost, packageActive } = require('../controllers/packages.controller');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares')
const { existPackageForId } = require("../helpers");

const router = Router();

router.get('/', [
    validateJWT,
    validateFields,
], packagesGet);

router.get('/public', getPackages);

router.get('/public/:slug', [
    check('slug', 'El slug es obligatorio').not().isEmpty(),
    validateFields,
], getPackage);

router.post('/', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], packagePost);

router.post('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPackageForId),
    validateFields
], packageActive);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPackageForId),
    validateFields
], packageGet);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPackageForId),
    validateFields
], packagePut);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPackageForId),
    validateFields
], packageDelete);

module.exports = router;
