const {v4: uuidv4} = require("uuid");
const path = require("path");

const fileUpload = (files, validExtensions = ['png', 'jpg', 'jpeg', 'gif'], directory = '') => {

    return new Promise((resolve, reject) => {
        const { file } = files;
        const cutName = file.name.split('.');
        const extension = cutName[cutName.length -1];
        // Validar extension
        if (!validExtensions.includes(extension)){
            return reject(`La extensión ${extension} no es valida, extensiones validas ${validExtensions}`);
        }

        const tempName = uuidv4() + '.' + extension;
        const uploadPath = path.join(__dirname , '../uploads/', directory, tempName);

        file.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }
            resolve(tempName);
        });
    });
}

const fileUpload2 = (files, validExtensions = ['png', 'jpg', 'jpeg', 'gif'], directory = '') => {

    return new Promise((resolve, reject) => {
        const { file2 } = files;
        const cutName = file2.name.split('.');
        const extension = cutName[cutName.length -1];
        // Validar extension
        if (!validExtensions.includes(extension)){
            return reject(`La extensión ${extension} no es valida, extensiones validas ${validExtensions}`);
        }

        const tempName = uuidv4() + '.' + extension;
        const uploadPath = path.join(__dirname , '../uploads/', directory, tempName);

        file2.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }
            resolve(tempName);
        });
    });
}

const fileUpload3 = (files, validExtensions = ['png', 'jpg', 'jpeg', 'gif'], directory = '') => {

    return new Promise((resolve, reject) => {
        const { file3 } = files;
        const cutName = file3.name.split('.');
        const extension = cutName[cutName.length -1];
        // Validar extension
        if (!validExtensions.includes(extension)){
            return reject(`La extensión ${extension} no es valida, extensiones validas ${validExtensions}`);
        }

        const tempName = uuidv4() + '.' + extension;
        const uploadPath = path.join(__dirname , '../uploads/', directory, tempName);

        file3.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }
            resolve(tempName);
        });
    });
}

const fileUpload4 = (files, validExtensions = ['png', 'jpg', 'jpeg', 'gif'], directory = '') => {

    return new Promise((resolve, reject) => {
        const { file4 } = files;
        const cutName = file4.name.split('.');
        const extension = cutName[cutName.length -1];
        // Validar extension
        if (!validExtensions.includes(extension)){
            return reject(`La extensión ${extension} no es valida, extensiones validas ${validExtensions}`);
        }

        const tempName = uuidv4() + '.' + extension;
        const uploadPath = path.join(__dirname , '../uploads/', directory, tempName);

        file4.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }
            resolve(tempName);
        });
    });
}

module.exports = {
    fileUpload,
    fileUpload2,
    fileUpload3,
    fileUpload4
}
